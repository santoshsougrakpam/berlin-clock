package com.inkglobal.techtest;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BerlinClockTest {
	
	 private BerlinClock berlinClock;
	 
	@Before
	public void init() {
		berlinClock = new BerlinClock();
	}

	@Test
	public void testSeconds() {
		Assert.assertEquals("Y", berlinClock.getSeconds(0));
		Assert.assertEquals("Y", berlinClock.getSeconds(2));
		Assert.assertEquals("O", berlinClock.getSeconds(5));
		Assert.assertEquals("Y", berlinClock.getSeconds(12));
	}

	@Test
	public void testFirsRowHours() {
		Assert.assertEquals("OOOO", berlinClock.getFirstRowHour(0));
		Assert.assertEquals("ROOO", berlinClock.getFirstRowHour(7));
		Assert.assertEquals("RRRO", berlinClock.getFirstRowHour(15));
		Assert.assertEquals("RRRR", berlinClock.getFirstRowHour(24));
	}
	
	@Test
	public void testSecondRowHours() {
		Assert.assertEquals("OOOO", berlinClock.getSecondRowHour(0));
		Assert.assertEquals("ROOO", berlinClock.getSecondRowHour(21));
		Assert.assertEquals("RRRR", berlinClock.getSecondRowHour(24));
	}
	
	@Test
	public void testFirstRowMins() {
		Assert.assertEquals("OOOOOOOOOOO", berlinClock.getFirstRowMinute(0));
		Assert.assertEquals("YYRYOOOOOOO", berlinClock.getFirstRowMinute(21));
		Assert.assertEquals("YYRYYRYYRYY", berlinClock.getFirstRowMinute(59));
	}

	@Test
	public void testSecondRowMins() {
		Assert.assertEquals("OOOO", berlinClock.getSecondRowMinute(0));
		Assert.assertEquals("YOOO", berlinClock.getSecondRowMinute(21));
		Assert.assertEquals("YYYY", berlinClock.getSecondRowMinute(59));
	}
	
	@Test
	public void testBerlinClockTime() {
		Assert.assertEquals("O RROO RRRO YYROOOOOOOO YYOO", berlinClock.getBerlinTime("13:17:01"));
		Assert.assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY", berlinClock.getBerlinTime("23:59:59"));
		Assert.assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO", berlinClock.getBerlinTime("24:00:00"));
	}
}
