package com.inkglobal.techtest;


public class BerlinClock {
	
	private static final int NO_OF_FIRST_ROW_HR_LIGHTS = 4;
	private static final int NO_OF_SECOND_ROW_HR_LIGHTS = 4;
	private static final int NO_OF_FIRST_ROW_MINS_LIGHTS = 11;
	private static final int NO_OF_SECOND_ROW_MINS_LIGHTS = 4;
	
	private static final String ON_RED_LIGHT_SYMBOL = "R";
	private static final String ON_YELLOW_LIGHT_SYMBOL = "Y";
	private static final String OFF_LIGHT_SYMBOL = "O";

	public String getBerlinTime(String timeIn24hrFormat) { 
		String[] time = timeIn24hrFormat.split(":");//hh:mm:ss
		return 
			new StringBuilder()
		   .append(getSeconds(Integer.valueOf(time[2]))).append(" ")
		   .append(getFirstRowHour(Integer.valueOf(time[0]))).append(" ")
		   .append(	getSecondRowHour(Integer.valueOf(time[0]))).append(" ")
		   .append(getFirstRowMinute(Integer.valueOf(time[1]))).append(" ")
		   .append(getSecondRowMinute(Integer.valueOf(time[1]))).toString();
	}
	
	protected String getSeconds(int seconds) {
		return seconds % 2 == 0 ? ON_YELLOW_LIGHT_SYMBOL : OFF_LIGHT_SYMBOL;
	}
	
	protected String getFirstRowHour(int hrs) {
		int noOfOn = getNoOfFirstRow(hrs);
		return convertToLights(NO_OF_FIRST_ROW_HR_LIGHTS, noOfOn, ON_RED_LIGHT_SYMBOL);
	}

	protected String getSecondRowHour(int hrs) {
		int noOfOn = hrs % 5;
		return convertToLights(NO_OF_SECOND_ROW_HR_LIGHTS, noOfOn, ON_RED_LIGHT_SYMBOL);
	}

	protected String getFirstRowMinute(int mins) {
		int noOfOn = getNoOfFirstRow(mins);
		String allYelloLights = convertToLights(NO_OF_FIRST_ROW_MINS_LIGHTS, noOfOn, ON_YELLOW_LIGHT_SYMBOL);
		return allYelloLights.replaceAll("YYY", "YYR");
	}
	
	protected String getSecondRowMinute(int mins) {
		int noOfOn = mins%5;
		return convertToLights(NO_OF_SECOND_ROW_MINS_LIGHTS, noOfOn, ON_YELLOW_LIGHT_SYMBOL);
	}
	
	private String convertToLights(int totalNoOfLights, int noOfOn, String onLightColor) {
		StringBuilder s = new StringBuilder();
		
		for(int i = 1; i <= totalNoOfLights; i++) {
			if(i <= noOfOn) {
				s.append(onLightColor);
			} else {
				s.append(OFF_LIGHT_SYMBOL);
			}
		}
		return s.toString();
	}

	private  int getNoOfFirstRow(int no) {
		// 5x + no%5 = no
		// hence x = (no - no%5)/5
		return (no - no%5) / 5;
	}

}
